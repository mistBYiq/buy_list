package org.example.shopping.entity;

public enum Category {
    FOOD("FOOD"),
    COSMETICS("COSMETICS"),
    NOT_SELECTED("NOT_SELECTED");

    private final String representation;

    Category(String representation) {
        this.representation = representation;
    }

    public String getRepresentation() {
        return representation;
    }
}
