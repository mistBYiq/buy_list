package org.example.shopping.controller;

import org.example.shopping.exception.ShoppingListEntityNotFoundException;
import org.example.shopping.exception.UserEntityNotFoundException;
import org.example.shopping.model.request.create.ShoppingListCreateRequest;
import org.example.shopping.model.request.update.ShoppingListUpdateRequest;
import org.example.shopping.model.response.ShoppingListModel;
import org.example.shopping.service.ShoppingListService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/shopping")
public class ShoppingListController {

    private ShoppingListService service;

    @GetMapping()
    public ResponseEntity getAllShoppingList(@RequestParam(defaultValue = "0") int page,
                                         @RequestParam(defaultValue = "20") int size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<ShoppingListModel> list = service.findAll(pageable);
            return ResponseEntity.ok(list);
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body("An error has occurred");
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity findShoppingListById(@PathVariable("id") Long id) {
        try {
            return ResponseEntity.ok(service.findById(id));
        } catch (ShoppingListEntityNotFoundException ex) {
            return ResponseEntity.badRequest().body(ex.getMessage());
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body("An error has occurred");
        }
    }

    @PostMapping()
    public ResponseEntity createShoppingList(@RequestBody ShoppingListCreateRequest model) {
        try {
            return ResponseEntity.ok(service.create(model));
        } catch (UserEntityNotFoundException ex) {
            return ResponseEntity.badRequest().body(ex.getMessage());
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body("An error has occurred");
        }
    }

    @PutMapping()
    public ResponseEntity updateShoppingList(@RequestBody ShoppingListUpdateRequest model) {
        try {
            return ResponseEntity.ok(service.update(model));
        } catch (ShoppingListEntityNotFoundException ex) {
            return ResponseEntity.badRequest().body(ex.getMessage());
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body("An error has occurred");
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteShoppingListById(@PathVariable("id") Long id) {
        try {
            service.deleteById(id);
            return ResponseEntity.ok("ShoppingList with id=" + id + " deleted successfully");
        } catch (ShoppingListEntityNotFoundException ex) {
            return ResponseEntity.badRequest().body(ex.getMessage());
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body("An error has occurred");
        }
    }
}
