package org.example.shopping.controller;

import org.example.shopping.exception.ProductEntityNotFoundException;
import org.example.shopping.model.request.create.ProductCreateRequest;
import org.example.shopping.model.request.update.ProductUpdateRequest;
import org.example.shopping.model.response.ProductModel;
import org.example.shopping.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/products")
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping()
    public ResponseEntity getAllProducts(@RequestParam(defaultValue = "0") int page,
                                      @RequestParam(defaultValue = "20") int size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<ProductModel> list = productService.findAll(pageable);
            return ResponseEntity.ok(list);
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body("An error has occurred");
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity findProductById(@PathVariable("id") Long id) {
        try {
            return ResponseEntity.ok(productService.findById(id));
        } catch (ProductEntityNotFoundException ex) {
            return ResponseEntity.badRequest().body(ex.getMessage());
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body("An error has occurred");
        }
    }

    @PostMapping()
    public ResponseEntity createProduct(@RequestBody ProductCreateRequest model) {
        try {
            return ResponseEntity.ok(productService.create(model));
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body("An error has occurred");
        }
    }

    @PutMapping()
    public ResponseEntity updateProduct(@RequestBody ProductUpdateRequest model) {
        try {
            return ResponseEntity.ok(productService.update(model));
        } catch (ProductEntityNotFoundException ex) {
            return ResponseEntity.badRequest().body(ex.getMessage());
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body("An error has occurred");
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteProductById(@PathVariable("id") Long id) {
        try {
            productService.deleteById(id);
            return ResponseEntity.ok("Product with id=" + id + " deleted successfully");
        } catch (ProductEntityNotFoundException ex) {
            return ResponseEntity.badRequest().body(ex.getMessage());
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body("An error has occurred");
        }
    }
}
