package org.example.shopping.controller;

import org.example.shopping.exception.UserEntityNotFoundException;
import org.example.shopping.model.request.create.UserCreateRequest;
import org.example.shopping.model.request.update.UserUpdateRequest;
import org.example.shopping.model.response.UserModel;
import org.example.shopping.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping()
    public ResponseEntity getAllUsers(@RequestParam(defaultValue = "0") int page,
                                      @RequestParam(defaultValue = "20") int size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<UserModel> list = userService.findAll(pageable);
            return ResponseEntity.ok(list);
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body("An error has occurred");
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity findUserById(@PathVariable("id") Long id) {
        try {
            return ResponseEntity.ok(userService.findById(id));
        } catch (UserEntityNotFoundException ex) {
            return ResponseEntity.badRequest().body(ex.getMessage());
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body("An error has occurred");
        }
    }

    @PostMapping()
    public ResponseEntity createUser(@RequestBody UserCreateRequest model) {
        try {
            return ResponseEntity.ok(userService.create(model));
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body("An error has occurred");
        }
    }

    @PutMapping()
    public ResponseEntity updateUser(@RequestBody UserUpdateRequest model) {
        try {
            return ResponseEntity.ok(userService.update(model));
        } catch (UserEntityNotFoundException ex) {
            return ResponseEntity.badRequest().body(ex.getMessage());
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body("An error has occurred");
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity updateUserById(@PathVariable("id") Long id,
                                         @RequestBody UserCreateRequest model) {
        try {
            return ResponseEntity.ok(userService.updateById(id, model));
        } catch (UserEntityNotFoundException ex) {
            return ResponseEntity.badRequest().body(ex.getMessage());
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body("An error has occurred");
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteUserById(@PathVariable("id") Long id) {
        try {
            userService.deleteById(id);
            return ResponseEntity.ok("User with id=" + id + " deleted successfully");
        } catch (UserEntityNotFoundException ex) {
            return ResponseEntity.badRequest().body(ex.getMessage());
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body("An error has occurred");
        }
    }
}
