package org.example.shopping.service.impl;

import com.sun.istack.NotNull;
import org.example.shopping.entity.User;
import org.example.shopping.exception.UserEntityNotFoundException;
import org.example.shopping.model.request.create.UserCreateRequest;
import org.example.shopping.model.request.update.UserUpdateRequest;
import org.example.shopping.model.response.UserModel;
import org.example.shopping.repository.UserRepository;
import org.example.shopping.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.stream.Collectors;

import static java.util.Optional.ofNullable;

@Service
public class UserServiceImpl implements UserService {

    private static final String ERR_MESSAGE = "User not found with id =";

    @Autowired
    private UserRepository userRepository;

    public Page<UserModel> findAll(Pageable pageable) {
        return new PageImpl<>(userRepository.findAll(pageable)
                .stream()
                .map(UserModel::toModel)
                .collect(Collectors.toList()));
    }

    public UserModel findById(Long id) throws UserEntityNotFoundException {
        return UserModel.toModel(userRepository.findById(id).orElseThrow(
                () -> new UserEntityNotFoundException(ERR_MESSAGE + id)));
    }

    public UserModel create(UserCreateRequest createModel) {
        User user = buildUser(createModel);
        return UserModel.toModel(userRepository.save(user));
    }

    public UserModel update(UserUpdateRequest request) throws UserEntityNotFoundException {
        User user = userRepository.findById(request.getId()).orElseThrow(
                () -> new UserEntityNotFoundException(ERR_MESSAGE + request.getId()));
        userUpdate(user, request);
        return UserModel.toModel(userRepository.save(user));
    }

    public UserModel updateById(Long id, UserCreateRequest request) throws UserEntityNotFoundException {
        User user = userRepository.findById(id).orElseThrow(
                () -> new UserEntityNotFoundException(ERR_MESSAGE + id));
        userUpdate(user, request);
        return UserModel.toModel(userRepository.save(user));
    }

    public void deleteById(Long id) throws UserEntityNotFoundException {
        userRepository.findById(id).orElseThrow(
                () -> new UserEntityNotFoundException(ERR_MESSAGE + id));
        userRepository.deleteById(id);
    }

    private void userUpdate(@NotNull User user, @NotNull UserCreateRequest request) {
        ofNullable(request.getName()).map(user::setName);
        ofNullable(request.getSurname()).map(user::setSurname);
        ofNullable(request.getEmail()).map(user::setEmail);
        ofNullable(request.getLogin()).map(user::setLogin);
        ofNullable(request.getPassword()).map(user::setPassword);
        user.setUpdated(LocalDateTime.now());
    }

    private User buildUser(UserCreateRequest model) {
        return new User()
                .setName(model.getName())
                .setSurname(model.getSurname())
                .setEmail(model.getEmail())
                .setLogin(model.getLogin())
                .setPassword(model.getPassword())
                .setCreated(LocalDateTime.now())
                .setUpdated(LocalDateTime.now())
                .setIsDeleted(Boolean.FALSE);
    }
}
