package org.example.shopping.service.impl;

import com.sun.istack.NotNull;
import org.example.shopping.entity.Product;
import org.example.shopping.exception.ProductEntityNotFoundException;
import org.example.shopping.model.request.create.ProductCreateRequest;
import org.example.shopping.model.request.update.ProductUpdateRequest;
import org.example.shopping.model.response.ProductModel;
import org.example.shopping.repository.ProductRepository;
import org.example.shopping.service.ProductService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

import static java.util.Optional.ofNullable;

@Service
public class ProductServiceImpl implements ProductService {

    private static final String ERR_MESSAGE = "Product not found with id =";

    private final ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public Page<ProductModel> findAll(Pageable pageable) {
        new PageImpl<>(productRepository.findAll(pageable)
                .stream()
                .map(ProductModel::toModel)
                .collect(Collectors.toList()));
        return null;
    }

    @Override
    public ProductModel findById(Long id) throws ProductEntityNotFoundException {
        Product product = productRepository.findById(id).orElseThrow(
                () -> new ProductEntityNotFoundException(ERR_MESSAGE + id)
        );
        return ProductModel.toModel(product);
    }

    @Override
    public ProductModel create(ProductCreateRequest request) {
        Product product = buildProduct(request);
        return ProductModel.toModel(productRepository.save(product));
    }

    @Override
    public ProductModel update(ProductUpdateRequest request) throws ProductEntityNotFoundException {
        Product product = productRepository.findById(request.getId()).orElseThrow(
                () -> new ProductEntityNotFoundException(ERR_MESSAGE + request.getId())
        );
        productUpdate(product, request);
        return ProductModel.toModel(product);
    }

    @Override
    public void deleteById(Long id) throws ProductEntityNotFoundException {
        productRepository.findById(id).orElseThrow(
                () -> new ProductEntityNotFoundException(ERR_MESSAGE + id)
        );
        productRepository.deleteById(id);
    }

    private Product buildProduct(ProductCreateRequest request) {
        return new Product()
                .setName(request.getName())
                .setDescription(request.getDescription())
                .setImportance(request.getImportance())
                .setPrice(request.getPrice())
                .setCategory(request.getCategory());
    }

    private void productUpdate(@NotNull Product product, @NotNull ProductCreateRequest request) {
        ofNullable(request.getName()).map(product::setName);
        ofNullable(request.getDescription()).map(product::setDescription);
        ofNullable(request.getImportance()).map(product::setImportance);
        ofNullable(request.getPrice()).map(product::setPrice);
        ofNullable(request.getCategory()).map(product::setCategory);
    }

}
