package org.example.shopping.service.impl;

import com.sun.istack.NotNull;
import org.example.shopping.entity.ShoppingList;
import org.example.shopping.entity.User;
import org.example.shopping.exception.ShoppingListEntityNotFoundException;
import org.example.shopping.exception.UserEntityNotFoundException;
import org.example.shopping.model.request.create.ShoppingListCreateRequest;
import org.example.shopping.model.request.create.UserCreateRequest;
import org.example.shopping.model.request.update.ShoppingListUpdateRequest;
import org.example.shopping.model.response.ShoppingListModel;
import org.example.shopping.model.response.ShoppingListWithProdModel;
import org.example.shopping.model.response.UserModel;
import org.example.shopping.repository.ProductRepository;
import org.example.shopping.repository.ShoppingListRepository;
import org.example.shopping.repository.UserRepository;
import org.example.shopping.service.ShoppingListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.stream.Collectors;

import static java.util.Optional.ofNullable;

@Service
public class ShoppingListServiceImpl implements ShoppingListService {

    private static final String ERR_MESSAGE = "ShoppingList not found with id =";

    @Autowired
    private ShoppingListRepository repository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProductRepository productRepository;

    @Override
    public Page<ShoppingListModel> findAll(Pageable pageable) {
        return new PageImpl<>(repository.findAll(pageable)
                .stream()
                .map(ShoppingListModel::toModel)
                .collect(Collectors.toList()));
    }

    @Override
    public ShoppingListModel findById(Long id) throws ShoppingListEntityNotFoundException {
        return ShoppingListModel.toModel(repository.findById(id).orElseThrow(
                () -> new ShoppingListEntityNotFoundException(ERR_MESSAGE + id)
        ));
    }

    @Override
    public ShoppingListModel create(ShoppingListCreateRequest request) throws UserEntityNotFoundException {
        User user = userRepository.findById(request.getUserId()).orElseThrow(
                () -> new UserEntityNotFoundException("User not found with id=" + request.getUserId()));
        ShoppingList list = buildShoppingList(request, user);
        return ShoppingListWithProdModel.toModel(list);
    }

    @Override
    public ShoppingListModel update(ShoppingListUpdateRequest request) throws ShoppingListEntityNotFoundException {
        ShoppingList shoppingList = repository.findById(request.getId()).orElseThrow(
                () -> new ShoppingListEntityNotFoundException(ERR_MESSAGE + request.getId())
        );
        shoppingListUpdate(shoppingList, request);
        return null;
    }

    @Override
    public void deleteById(Long id) throws ShoppingListEntityNotFoundException {
        repository.findById(id).orElseThrow(
                () -> new ShoppingListEntityNotFoundException(ERR_MESSAGE + id)
        );
        repository.deleteById(id);
    }

    private ShoppingList buildShoppingList(ShoppingListCreateRequest request, User user) {
        return new ShoppingList()
                .setName(request.getName())
                .setDescription(request.getDescription())
                .setUser(user)
                .setDeadline(request.getDeadline())
                .setCreated(LocalDateTime.now())
                .setUpdated(LocalDateTime.now())
                .setIsDeleted(Boolean.FALSE)
                .setTotalPrice(0);
    }

    private void shoppingListUpdate(@NotNull ShoppingList shoppingList, @NotNull ShoppingListCreateRequest request) {
        ofNullable(request.getName()).map(shoppingList::setName);
        ofNullable(request.getDescription()).map(shoppingList::setDescription);
        ofNullable(request.getDeadline()).map(shoppingList::setDeadline);
        shoppingList.setUpdated(LocalDateTime.now());
    }
}
