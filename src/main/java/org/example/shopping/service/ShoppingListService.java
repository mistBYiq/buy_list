package org.example.shopping.service;

import org.example.shopping.exception.ShoppingListEntityNotFoundException;
import org.example.shopping.exception.UserEntityNotFoundException;
import org.example.shopping.model.request.create.ShoppingListCreateRequest;
import org.example.shopping.model.request.update.ShoppingListUpdateRequest;
import org.example.shopping.model.response.ShoppingListModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ShoppingListService {
    Page<ShoppingListModel> findAll(Pageable pageable);

    ShoppingListModel findById(Long id) throws ShoppingListEntityNotFoundException;

    ShoppingListModel create(ShoppingListCreateRequest request) throws UserEntityNotFoundException;

    ShoppingListModel update(ShoppingListUpdateRequest request) throws ShoppingListEntityNotFoundException;

    void deleteById(Long id) throws ShoppingListEntityNotFoundException;
}
