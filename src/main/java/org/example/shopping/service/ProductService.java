package org.example.shopping.service;

import org.example.shopping.exception.ProductEntityNotFoundException;
import org.example.shopping.model.request.create.ProductCreateRequest;
import org.example.shopping.model.request.update.ProductUpdateRequest;
import org.example.shopping.model.response.ProductModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ProductService {
    Page<ProductModel> findAll(Pageable pageable);

    ProductModel findById(Long id) throws ProductEntityNotFoundException;

    ProductModel create(ProductCreateRequest request);

    ProductModel update(ProductUpdateRequest request) throws ProductEntityNotFoundException;

    void deleteById(Long id) throws ProductEntityNotFoundException;
}
