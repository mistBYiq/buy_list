package org.example.shopping.service;

import org.example.shopping.exception.UserEntityNotFoundException;
import org.example.shopping.model.request.create.UserCreateRequest;
import org.example.shopping.model.request.update.UserUpdateRequest;
import org.example.shopping.model.response.UserModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserService {
    Page<UserModel> findAll(Pageable pageable);

    UserModel findById(Long id) throws UserEntityNotFoundException;

    UserModel create(UserCreateRequest request);

    UserModel update(UserUpdateRequest request) throws UserEntityNotFoundException;

    UserModel updateById(Long id, UserCreateRequest request) throws UserEntityNotFoundException;

    void deleteById(Long id) throws UserEntityNotFoundException;
}
