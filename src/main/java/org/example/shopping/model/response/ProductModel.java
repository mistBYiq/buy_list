package org.example.shopping.model.response;

import lombok.Data;
import lombok.experimental.Accessors;
import org.example.shopping.entity.Importance;
import org.example.shopping.entity.Product;

@Data
@Accessors(chain = true)
public class ProductModel {
    private Long id;
    private String name;
    private String description;
    private String importance;
    private Integer price;
    private String category;

    public static ProductModel toModel(Product product) {
        ProductModel model = new ProductModel();
        model.setId(product.getId());
        model.setName(product.getName());
        model.setDescription(product.getDescription());
        model.setImportance(product.getImportance().getRepresentation());
        model.setPrice(product.getPrice());
        model.setCategory(product.getCategory().getRepresentation());
        return model;
    }
}
