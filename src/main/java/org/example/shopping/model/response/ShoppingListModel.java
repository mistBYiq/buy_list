package org.example.shopping.model.response;

import lombok.Data;
import lombok.experimental.Accessors;
import org.example.shopping.entity.ShoppingList;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Accessors(chain = true)
public class ShoppingListModel {
    private Long id;
    private String name;
    private String description;
    private UserModel user;
    private LocalDateTime deadline;
    private LocalDateTime created;
    private LocalDateTime updated;
    private Boolean isDeleted;
    private Integer totalPrice;
   // private List<ProductModel> products;

    public static ShoppingListModel toModel(ShoppingList entity) {
        ShoppingListModel model = new ShoppingListModel();
        model.setId(entity.getId());
        model.setName(entity.getName());
        model.setDescription(entity.getDescription());
        model.setUser(UserModel.toModel(entity.getUser()));
        model.setDeadline(entity.getDeadline());
        model.setCreated(entity.getCreated());
        model.setUpdated(entity.getUpdated());
        model.setIsDeleted(entity.getIsDeleted());
        model.setTotalPrice(entity.getTotalPrice());
//        model.setProducts(entity.getProducts().stream()
//                .map(ProductModel::toModel)
//                .sorted()
//                .collect(Collectors.toList()));

        return model;
    }
}
