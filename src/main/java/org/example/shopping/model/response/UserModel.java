package org.example.shopping.model.response;

import lombok.Data;
import lombok.experimental.Accessors;
import org.example.shopping.entity.User;

import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
public class UserModel {
    private Long id;
    private String name;
    private String surname;
    private String email;
    private String login;
    private String password;
    private LocalDateTime created;
    private LocalDateTime updated;
    private Boolean isDeleted;

    public static UserModel toModel(User user) {
        UserModel model = new UserModel();
        model.setId(user.getId());
        model.setName(user.getName());
        model.setSurname(user.getSurname());
        model.setEmail(user.getEmail());
        model.setLogin(user.getLogin());
        model.setPassword(user.getPassword());
        model.setCreated(user.getCreated());
        model.setUpdated(user.getUpdated());
        model.setIsDeleted(user.getIsDeleted());

        return model;
    }
}
