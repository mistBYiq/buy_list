package org.example.shopping.model.response;

import lombok.Data;
import lombok.experimental.Accessors;
import org.example.shopping.entity.ShoppingList;

import java.util.List;
import java.util.stream.Collectors;

@Data
@Accessors(chain = true)
public class ShoppingListWithProdModel extends ShoppingListModel {
    private List<ProductModel> products;

    public static ShoppingListWithProdModel toModel(ShoppingList entity) {
        ShoppingListWithProdModel model = (ShoppingListWithProdModel) ShoppingListModel.toModel(entity);
        model.setProducts(entity.getProducts().stream()
                .map(ProductModel::toModel)
                .sorted()
                .collect(Collectors.toList()));
        return model;
    }
}
