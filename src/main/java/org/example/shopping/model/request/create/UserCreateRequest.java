package org.example.shopping.model.request.create;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UserCreateRequest {
    private String name;
    private String surname;
    private String email;
    private String login;
    private String password;
}
