package org.example.shopping.model.request.create;

import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
public class ShoppingListCreateRequest {
    private String name;
    private String description;
    private Long userId;
    private LocalDateTime deadline;
}
