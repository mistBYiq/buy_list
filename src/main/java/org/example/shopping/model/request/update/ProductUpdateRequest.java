package org.example.shopping.model.request.update;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.example.shopping.model.request.create.ProductCreateRequest;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class ProductUpdateRequest extends ProductCreateRequest {
    private Long id;
}
