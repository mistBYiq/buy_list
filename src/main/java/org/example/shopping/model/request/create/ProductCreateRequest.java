package org.example.shopping.model.request.create;

import lombok.Data;
import lombok.experimental.Accessors;
import org.example.shopping.entity.Category;
import org.example.shopping.entity.Importance;

@Data
@Accessors(chain = true)
public class ProductCreateRequest {
    private String name;
    private String description;
    private Importance importance;
    private Integer price;
    private Category category;
}
