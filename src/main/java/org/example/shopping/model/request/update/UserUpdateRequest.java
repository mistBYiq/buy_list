package org.example.shopping.model.request.update;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.example.shopping.model.request.create.UserCreateRequest;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class UserUpdateRequest extends UserCreateRequest {
    private Long id;
}
